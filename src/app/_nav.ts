import { INavData } from '@coreui/angular';

export const navItems: INavData[] = [
  {
    name: 'Dashboard',
    url: '/dashboard',
    icon: 'icon-speedometer',
  },
  {
    name: 'Autores',
    url: '/base/autores',
    icon: 'icon-list',
  },
  {
    name: 'Libros',
    url: '/base/libros',
    icon: 'icon-list',
  },
];
