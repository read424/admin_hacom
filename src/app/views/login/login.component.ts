import { Component, OnInit, NgZone } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { AlertComponent } from 'ngx-bootstrap/alert';
import { AdminService } from '../../services/admin.service';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-dashboard',
  templateUrl: 'login.component.html'
})
export class LoginComponent implements OnInit { 

  public formSubmited = false;
  public loginForm = this.fb.group({
    username: [ '',  Validators.required],
    password: ['', Validators.required],
  });

  alerts: any[] = [{
    // type: 'success',
    // msg: `Well done! You successfully read this important alert message. (added: ${new Date().toLocaleTimeString()})`,
    // timeout: 5000
  }];
 


  constructor( private router: Router,
    private fb: FormBuilder,
    private ngZone: NgZone,
    private adminService: AdminService ) { }


  ngOnInit(): void {

  }




  loginAdministrador() {
    this.adminService.loginAdministrador(this.loginForm.value)
    .subscribe(resp => {
      if(resp.num_rows == 1) {
        this.alerts.push({
          type: 'success',
          msg: `${resp.message}`,
          timeout: 2000
        });
        localStorage.setItem('tkn_tlm', resp.token);
        this.router.navigate(['/dashboard']);
      } else {
        // Swal.fire(` ${resp.message}` , 'danger');

        Swal.fire({
          position: 'center',
          icon: 'error',
          title: `${resp.message}`,
          showConfirmButton: false,
          timer: 1500
        })


      }
    }, (err) =>   {
      Swal.fire('Error',  err.error.msg, 'error');
    });

  }


  onClosed(dismissedAlert: AlertComponent): void {
    this.alerts = this.alerts.filter(alert => alert !== dismissedAlert);
  }





}

