import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ClientsService } from '../../../services/clients.service';
import { ReporteService } from '../../../services/reporte.service';

@Component({
  selector: 'app-clientes',
  templateUrl: './clientes.component.html',
  styleUrls: ['./clientes.component.scss']
})
export class ClientesComponent implements OnInit {

  public data: any []= [];

  constructor(public clientService: ClientsService,
              public router: Router,
              public reporteServices: ReporteService) { }


  ngOnInit(): void {
    this.getData();
  }

  getData(){
    this.clientService.getClients().subscribe(resp => {
      console.log(resp['rows']);
      this.data = resp['rows'];
    })
  }



  irDetalle(id){
      this.router.navigateByUrl(`base/clientes/details/${id}`);
  }
 

  exportrUser() {
    this.reporteServices.exportAsExcelFile(this.data, 'UsuariosTLMAPP');
 }


}
