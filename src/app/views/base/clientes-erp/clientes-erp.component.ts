import { Component, OnInit, ViewChild } from '@angular/core';
import { ClientsService } from '../../../services/clients.service';
import { ModalDirective } from 'ngx-bootstrap/modal';
import { NgSearchFilterService } from 'ng-search-filter';
import { Router } from '@angular/router';
import { ReporteService } from '../../../services/reporte.service';

@Component({
  selector: 'app-clientes-erp',
  templateUrl: './clientes-erp.component.html',
  styleUrls: ['./clientes-erp.component.scss']
})
export class ClientesErpComponent implements OnInit {


  @ViewChild('largeModal') public largeModal: ModalDirective;


  public data: any[] = [];

  public textoBuscar;


  public dataEdit: any[] = [];
  public mostrar: boolean = false;

  searchText = '';

  constructor(public clientService: ClientsService,
    private _ngSearchFilterService: NgSearchFilterService,
    public router: Router,
    public reporteServices: ReporteService) { 

      this._ngSearchFilterService.setDefaultLang('en');
    }

    

  ngOnInit(): void {
    this.getData();
  }

  

  getData(){
    this.clientService.getClientsERP().subscribe(resp => {
      console.log(resp['rows']);
      this.data = resp['rows'];
    })
  }


  buscar(event){
    console.log(event)
    this.textoBuscar =  event;
  }



  openModal(data){
    this.largeModal.show();
    this.dataEdit = data;
  }

  dismiss(){
    this.largeModal.hide();
  }


  editInfo(id){
    this.router.navigateByUrl(`base/clientes/details/${id}`);
  }



  exportrUser() {
    this.reporteServices.exportAsExcelFile(this.data, 'ClientesTLM');
 }

}
