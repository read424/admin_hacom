// Angular
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NgModule } from '@angular/core';

// Tabs Component
import { TabsModule } from 'ngx-bootstrap/tabs';

// Carousel Component
import { CarouselModule } from 'ngx-bootstrap/carousel';

// Collapse Component
import { CollapseModule } from 'ngx-bootstrap/collapse';

// Dropdowns Component
import { BsDropdownModule } from 'ngx-bootstrap/dropdown';

// Pagination Component
import { PaginationModule } from 'ngx-bootstrap/pagination';

// Popover Component
import { PopoverModule } from 'ngx-bootstrap/popover';

// Progress Component
import { ProgressbarModule } from 'ngx-bootstrap/progressbar';

// Tooltip Component
import { TooltipModule } from 'ngx-bootstrap/tooltip';

// navbars
import { NavbarsComponent } from './navbars/navbars.component';

// Components Routing
import { BaseRoutingModule } from './base-routing.module';
import { ClientesComponent } from './clientes/clientes.component';
import { NotificationsComponent } from './notifications/notifications.component';
import { ClientesErpComponent } from './clientes-erp/clientes-erp.component';
import { NgxPaginationModule } from 'ngx-pagination';
import { PipesModule } from '../../pipes/pipes.module';
import { ModalModule } from 'ngx-bootstrap/modal';
import { NgxBootstrapMultiselectModule } from 'ngx-bootstrap-multiselect';
import { NgMultiSelectDropDownModule } from 'ng-multiselect-dropdown';
import { DetailsComponent } from './details/details.component';
import { NgSearchFilterModule } from 'ng-search-filter';
import { VisitasComponent } from './visitas/visitas.component';
import { DetailsvisComponent } from './detailsvis/detailsvis.component';
import { AutoresComponent } from './autores/autores.component';
import { BooksComponent } from './books/books.component';
import { DetailsbookComponent } from './books/detailsbook.component';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    BaseRoutingModule,
    BsDropdownModule.forRoot(),
    CarouselModule.forRoot(),
    CollapseModule.forRoot(),
    PaginationModule.forRoot(),
    PopoverModule.forRoot(),
    ProgressbarModule.forRoot(),
    TooltipModule.forRoot(),
    NgxPaginationModule,
    PipesModule,
    ModalModule.forRoot(),
    ReactiveFormsModule,
    NgMultiSelectDropDownModule.forRoot(),
    NgSearchFilterModule,
    TabsModule.forRoot()
  ],
  declarations: [
    NavbarsComponent,
    ClientesComponent,
    NotificationsComponent,
    ClientesErpComponent,
    DetailsComponent,
    VisitasComponent,
    DetailsvisComponent,
    AutoresComponent,
    BooksComponent,
    DetailsbookComponent,
    
  ]
})
export class BaseModule { }
