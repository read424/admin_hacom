import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { VisitaService } from '../../../services/visita.service';
import { Validators, FormBuilder } from '@angular/forms';
import Swal from 'sweetalert2';
import { NotificationsService } from '../../../services/notifications.service';

@Component({
  selector: 'app-detailsvis',
  templateUrl: './detailsvis.component.html',
  styleUrls: ['./detailsvis.component.scss']
})
export class DetailsvisComponent implements OnInit {

  public idDetail = this.actRouter.snapshot.paramMap.get('id');
  public data: [] = [];
  public visitantes: [] = [];

  status = [
    { 'name': 'Pendiente', 'cod': 1},
    { 'name': 'En planta', 'cod': 2},
    { 'name': 'Completado','cod': 3}
  ]



  public formSubmited = false;
  public registerForm = this.fb.group({
    id_visita: [ this.idDetail,  Validators.required],
    status: [ '',  Validators.required],
    init_visit: ['', Validators.required],
    end_visit: ['', Validators.required],
  });
  

  constructor(public actRouter: ActivatedRoute,
              public visitaService: VisitaService,
              private fb: FormBuilder,
              public push: NotificationsService) { }

  ngOnInit(): void {
    this.visitaService.detailVisita(this.idDetail).subscribe(resp => {
      this.data = resp.rows[0];
    });


    this.visitaService.consultarVisitantes(this.idDetail).subscribe(resp => this.visitantes = resp['rows'] );


  }





  updateForm(){
    Swal.fire({
      title: 'Actualizar visita?',
      text: `Esta seguro de actulizar visita?` ,
      icon: 'warning',
      showCancelButton: true,
      confirmButtonText: 'Si, actualizar!'
    }).then((result) => {
      if (result.value) {
        this.visitaService.updateVisita(this.registerForm.value)
        .subscribe(resp => {
          console.log(resp);
          Swal.fire('Usuario Guardado', `${resp.message}` , 'success');

        },(err) =>   {
          Swal.fire('Error', `${err.error.msg}` , 'error');

        });
      }
    })
  }


  pushStatus(data){
    Swal.fire({
      title: 'Desea Notificar?',
      text: `Enviar Notificación a  ${data.razon_social} del estado actual de su visita a planta?` ,
      icon: 'warning',
      showCancelButton: true,
      confirmButtonText: 'Si, notificar!'
    }).then((result) => {
      if (result.value) {


        const title = 'ACTUALIZACIÓN DE VISITA'
        const content = `Hola ${data.razon_social} se encuentra en planta?`
        const content2 = `Muy bien ${data.razon_social} la visita finalizó. Muchas gracias`


        if(data.status == 1 ) {
          // pendiente
          console.log('pendiente');
        } else if(data.status == 2){
          console.log('planta');
          // planta
            this.push.pushNotificationsAPP(title, content, [ data.token_push]);
            Swal.fire(title, content, 'success');
            this.push.savePush({'id_cliente': this.idDetail, 'title': title, 'descriptions': content}).subscribe(resp => console.log(resp));
        } else if(data.status == 3){
          // finalizo
            this.push.pushNotificationsAPP(title, content2, [ data.token_push]);
            Swal.fire(title,content2 , 'success');
            this.push.savePush({'id_cliente': this.idDetail, 'title': title, 'descriptions': content2}).subscribe(resp => console.log(resp));
        }

        // this.push.pushNotificationsAPP(`ACTUALIZACIÓN DE VISITA'`, `Su estado actual es ${data.status}`, data.token_push);
        // this.visitaService.updateVisita(this.registerForm.value)
        // .subscribe(resp => {
        //   console.log(resp);
        //   Swal.fire('Usuario Guardado', `${resp.message}` , 'success');

        // },(err) =>   {
        //   Swal.fire('Error', `${err.error.msg}` , 'error');

        // });
      }
    })

  }







}
