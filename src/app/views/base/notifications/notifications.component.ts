import { Component, OnInit } from '@angular/core';
import { IMultiSelectOption } from 'ngx-bootstrap-multiselect';
import { Validators, FormBuilder } from '@angular/forms';
import { ClientsService } from '../../../services/clients.service';
import { NotificationsService } from '../../../services/notifications.service';
import Swal from 'sweetalert2';


@Component({
  selector: 'app-notifications',
  templateUrl: './notifications.component.html',
  styleUrls: ['./notifications.component.scss']
})
export class NotificationsComponent implements OnInit {

  dropdownList = [];
  selectedItems = [];
  dropdownSettings = {};

  public data: any []= [];
  public push: any []= [];
  
  public formSubmited = false;
  public registerForm = this.fb.group({
    title: [ '',  Validators.required],
    descriptions: ['', Validators.required],
    id_cliente: ['0', Validators.required],
    registration_ids: ['', Validators.required]
  });

  public notifications: Notification[] = [];

  

  constructor(private fb: FormBuilder,
              public clientService: ClientsService,
              public pushServices: NotificationsService ) { }
  
  ngOnInit() {
    this.getData();
    this.getPush();
    this.dropdownSettings = {
      singleSelection: false,
      idField: 'token_push',
      textField: 'no_razon',
      selectAllText: 'Seleccionar todos',
      unSelectAllText: 'Quitar todos',
      searchPlaceholderText: 'Buscar en la lista...',
      itemsShowLimit: 5,
      allowSearchFilter: true
    };
  }

  getData(){
    this.clientService.getClients().subscribe(resp => {
      this.data = resp['rows'];
    })
  }

  getPush(){
    this.pushServices.getPush().subscribe(resp => {
      this.push = resp['rows'];
    })
  }


  onItemSelect(item: any) {
    console.log(item.item_id);
  }
  onSelectAll(items: any) {
    console.log(items.item_id);
  }

  sendPush(){
    Swal.fire({
      title: 'Enviar Notificación',
      text: `Esta seguro de crear y enviar notificación?` ,
      icon: 'info',
      showCancelButton: true,
      confirmButtonText: 'Si, enviar!'
    }).then((result) => {
      if (result.value) {
        const data = this.registerForm.controls['registration_ids'].value;
        const arrTemp = [];
        data.forEach(data => {
          const linea = `${data.token_push}`;
          arrTemp.push(linea);
        });

        //enviar push
        const title = this.registerForm.controls['title'].value;
        const description = this.registerForm.controls['descriptions'].value;
        this.pushServices.pushNotificationsAPP(title, description, arrTemp);

        this.pushServices.savePush(this.registerForm.value).subscribe(resp => {
        this.registerForm.controls['title'].setValue('');
        this.registerForm.controls['descriptions'].setValue('');
        this.registerForm.controls['registration_ids'].setValue('');
        
        Swal.fire(` Notificación enviada correctamente` , 'success');
        this.getPush();
        }, (err) =>   {
          console.log(err)
        });
      }
    })
  }




}