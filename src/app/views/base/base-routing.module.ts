import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { NavbarsComponent } from './navbars/navbars.component';
import { UserGuard } from '../../guards/user.guard';
import { ClientesComponent } from './clientes/clientes.component';
import { NotificationsComponent } from './notifications/notifications.component';
import { ClientesErpComponent } from './clientes-erp/clientes-erp.component';
import { DetailsComponent } from './details/details.component';
import { VisitasComponent } from './visitas/visitas.component';
import { DetailsvisComponent } from './detailsvis/detailsvis.component';
import { AutoresComponent } from './autores/autores.component';
import { BooksComponent } from './books/books.component';
import { DetailsbookComponent } from './books/detailsbook.component';

const routes: Routes = [
  {
    path: '',
    data: {
      title: 'Base'
    },
    children: [
      {
        path: '',
        redirectTo: 'cards'
      },
      {
        path: 'autores',
        component: AutoresComponent,
        data: {
          title: 'Autores'
        },
        // canActivate: [UserGuard]
      },
      {
        path: 'autores/details/:id',
        component: DetailsComponent,
        data: {
          title: 'Registro Autores'
        },
        // canActivate: [UserGuard]
      },
      {
        path: 'libros',
        component: BooksComponent,
        data: {
          title: 'Libros'
        },
        // canActivate: [UserGuard]
      },
      {
        path: 'libros/details/:id',
        component: DetailsbookComponent,
        data: {
          title: 'Registro de Libro'
        },
        // canActivate: [UserGuard]
      },
      {
        path: 'navbars',
        component: NavbarsComponent,
        data: {
          title: 'Navbars'
        }
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class BaseRoutingModule {}
