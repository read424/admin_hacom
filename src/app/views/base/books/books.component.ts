import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Observable, OperatorFunction } from 'rxjs';
import { debounceTime, distinctUntilChanged, map } from 'rxjs/operators';
import { BooksService } from '../../../services/books.service';


@Component({
  selector: 'app-books',
  templateUrl: './books.component.html',
  styleUrls: ['./books.component.scss']
})

export class BooksComponent implements OnInit {

  public data: any [] = [];

  constructor(public booksServices: BooksService,
      public router: Router) { }

  ngOnInit(): void {
    this.getData();
  }

  edit_books(id){
    this.router.navigateByUrl(`base/libros/details/${id}`);
  }

  getData(){
    this.data = this.booksServices.getBooks();
  }


}
