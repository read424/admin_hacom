import { Component, OnInit } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { Observable, OperatorFunction } from 'rxjs';
import { debounceTime, distinctUntilChanged, map } from 'rxjs/operators';
import { ActivatedRoute } from '@angular/router';
import { BooksService } from '../../../services/books.service';

const authors = [
  {id_autor:1, name_autor:'George Walton, Lucas Jr.'}, {id_autor:2, name_autor:'J.K. Rowling'},
  {id_autor:3, name_autor:'George R. R. Martin'}, {id_autor:4, name_autor:''},
  {id_autor:5, name_autor:'J.R.R. Tolkien'}, {id_autor:6, name_autor:'E.L. James'}
];

@Component({
  selector: 'app-detailsbook',
  templateUrl: './detailsbook.component.html',
  styleUrls: ['./detailsbook.component.scss']
})
export class DetailsbookComponent implements OnInit {
  states: string[] = [
    'Alabama', 'Alaska', 'Arizona', 'Arkansas', 'California', 'Colorado', 'Connecticut', 'Delaware', 
    'Florida', 'Georgia', 'Hawaii', 'Idaho', 'Illinois', 'Indiana', 'Iowa', 'Kansas', 'Kentucky', 
    'Louisiana', 'Maine', 'Maryland', 'Massachusetts', 'Michigan', 'Minnesota', 'Mississippi',
    'Missouri', 'Montana', 'Nebraska', 'Nevada', 'New Hampshire', 'New Jersey', 'New Mexico', 'New York', 
    'North Dakota', 'North Carolina', 'Ohio', 'Oklahoma', 'Oregon', 'Pennsylvania', 'Rhode Island', 
    'South Carolina', 'South Dakota', 'Tennessee', 'Texas', 'Utah', 'Vermont', 'Virginia', 'Washington', 
    'West Virginia', 'Wisconsin', 'Wyoming'
  ];
  selected: string;
  public idLibro = this.actRouter.snapshot.paramMap.get('id');
  public model: any;

  search: OperatorFunction<string, readonly {id_autor, name_autor}[]> = (text$: Observable<string>)=>
    text$.pipe(
      debounceTime(20),
      distinctUntilChanged(),
      map(term => term.length<2 ? []
        : authors.filter(v => v.name_autor.toLowerCase().indexOf(term.toLowerCase())>-1).slice(0,10)
      )
    )

  formatter = (x: {name_autor: string}) => x.name_autor;


  public formSubmited = false;
  public registerBook = this.fb.group({
    titulo: [ '', Validators.required],
    descripcion: ['', Validators.required]
  });

  constructor(public actRouter: ActivatedRoute,
              public booksServices: BooksService,
              private fb: FormBuilder
          ) { }

  ngOnInit(): void {
    console.log(this.idLibro);
  }

}
