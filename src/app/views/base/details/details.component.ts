import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { ClientsService } from '../../../services/clients.service';
import { IUbigeo, Representante, Cliente } from '../../../interfaces/interfaces';
import Swal from 'sweetalert2';
import { FormBuilder } from '@angular/forms';

@Component({
  selector: 'app-details',
  templateUrl: './details.component.html',
  styleUrls: ['./details.component.scss']
})
export class DetailsComponent implements OnInit {

  public idDetail = this.actRouter.snapshot.paramMap.get('id');

  public id_login: '';
  public status: '';
  public razon: ''
  public representantes: [] = [];
  public contactos: [] = [];
  public user: [] = [];
  public doc: [] = [];
  public dep: IUbigeo[] = [];
  public pro: IUbigeo[] = [];
  public dis: IUbigeo[] = [];

  proveedor = [ { 'proveedor': 'Si', 'valor': true },
                { 'proveedor': 'No', 'valor': false} ]

  st = [ { 'name': 'Activo', 'cod': 1 },
         { 'name': 'Desactivo', 'cod': -1 } ]

  public formSubmited = false;
  public registerForm = this.fb.group({
         is_proveedor: [''],
         id_cliente: [this.idDetail],
         id_tipdocumento: [''],
         num_doc: [''],
         no_alias: [''],
         no_apepat: [''],
         no_apemat: [''],
         no_nombres: [''],
         no_razon: [''],
         no_dir: [''],
         co_depart: [''],
         co_provin: [''],
         co_distri: [''],
         nu_telefono: [''],
         nu_celular: [''],
         no_correo: [''],
         no_web: [''],
         fe_const: [''],
         fe_inicio: [''],
         no_cont: [''],
         no_aget: [''],
         razon_social: [''],
         representantes: [''],
         contactos: ['']
  });

  constructor(public actRouter: ActivatedRoute,
              public clientService: ClientsService,
              private fb: FormBuilder) {
                this.clientService.getDepartamentos().subscribe(resp => {
                  this.dep = resp['rows'];
                })
                this.clientService.getProvincias().subscribe(resp => {
                  this.pro = resp['rows'];
                })
                this.clientService.getDistritos().subscribe(resp => {
                  this.dis = resp['rows'];
                })
              }

  ngOnInit(): void {
    this.clientService.detailClient(this.idDetail).subscribe(resp => {
      this.user = resp['data'];
      this.representantes = resp.data['representantes'];
      this.contactos = resp.data['contactos'];
    });
    this.clientService.getRole().subscribe(resp => {
      this.doc = resp['rows'];
    });
    this.clientService.detailUserApp(this.idDetail).subscribe(resp => {
      this.id_login = resp.rows[0].id_login;
      this.status = resp.rows[0].status;
    });
  }

  
  updateStatus(){
    this.clientService.updateUserApp({"id_login": this.id_login, "status":  event}).subscribe(resp => {
      console.log(resp);
    });
  }
 

  cambio(event){
    this.clientService.updateUserApp({"id_login": this.id_login, "status":  event}).subscribe(resp => {
      Swal.fire({
        position: 'center', icon: 'success',
        title: `${resp['message']}`,
        showConfirmButton: false, timer: 1000
      })
    });
  }


  saveInformation(){
    let data:Cliente = this.registerForm.value;
    data.representantes = this.representantes;
    data.contactos = this.contactos;
    // console.log(data);
    this.clientService.saveClient(data).subscribe(resp => {
      Swal.fire({
        position: 'center', icon: 'success',
        title: `${resp['message']}`,
        showConfirmButton: false, timer: 1000
      })
    });
  }







}
