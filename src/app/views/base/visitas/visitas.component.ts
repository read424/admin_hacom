import { Component, OnInit } from '@angular/core';
import { VisitaService } from '../../../services/visita.service';
import { Router } from '@angular/router';
import { ReporteService } from '../../../services/reporte.service';

@Component({
  selector: 'app-visitas',
  templateUrl: './visitas.component.html',
  styleUrls: ['./visitas.component.scss']
})
export class VisitasComponent implements OnInit {

  public data: any[] = [];
  public textoBuscar;

  constructor(public visitaService: VisitaService,
    public router: Router,
    public reporteServices: ReporteService) { }

  ngOnInit(): void {
    this.visitaService.getVisitas().subscribe(resp => {
      this.data = resp['rows'];
    })
  }

  irDetalle(id){
    this.router.navigateByUrl(`base/visitas/detailsvis/${id}`);
  }

  exportrUser() {
    this.reporteServices.exportAsExcelFile(this.data, 'VisitasTLM');
 }


}
