import { Component, OnInit, TemplateRef } from '@angular/core';
import { Router } from '@angular/router';
import { AutoresService } from '../../../services/autores.service';
import { BsModalRef, BsModalService } from 'ngx-bootstrap/modal';

@Component({
  selector: 'app-autores',
  templateUrl: './autores.component.html',
  styleUrls: ['./autores.component.scss']
})
export class AutoresComponent implements OnInit {

  modalWin: BsModalRef;
  public data: any [] = [];

  constructor(public authorsService: AutoresService,
              public router: Router,
              private modalService: BsModalService
            ) { }

  ngOnInit(): void {
    this.getData();
  }

  openModal(template: TemplateRef<any>){
    this.modalWin = this.modalService.show(template);
  }

  getData(){
    this.data = this.authorsService.getAuthors();
  }

  view(id){
    this.router.navigateByUrl(`base/clientes/details/${id}`);
  }

  nuevoUser() {
    // this.reporteServices.exportAsExcelFile(this.data, 'UsuariosTLMAPP');
  }

  remover(){

  }

}
