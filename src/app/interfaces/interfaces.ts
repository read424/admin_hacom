


export interface Notifications {
    title?: string,
    description?: string,
    registration_ids?: any
}




export interface IUbigeo {
    co_depart: string;
    co_provin: string;
    co_distri: string;

    no_depart: string;
    no_provin: string;
    no_distri: string;
}



export interface Representante {
    id_representante: string;
    no_representante: string;
    cargo_repre: string;
    dni_repre: string;
    dropenabled: number;
}

export interface Contacto {
    id_contacto: string;
    no_contacto: string;
    cargo_contacto: string;
    nu_cel_contacto: string;
    correo_contacto: string;
    drop_contacto: number;
}

export interface Cliente {
    is_proveedor: boolean;
    id_cliente: string;
    id_tipdocumento: string;
    num_doc: string;
    no_alias: string;
    no_apepat: string;
    no_apemat: string;
    no_nombres: string;
    no_razon: string;
    no_dir: string;
    co_depart: string;
    co_provin: string;
    co_distri: string;
    nu_telefono: string;
    nu_celular: string;
    no_correo: string;
    no_web: string;
    fe_const: string;
    fe_inicio: string;
    no_cont: string;
    no_aget: string;
    razon_social: string;
    representantes: Representante[];
    contactos: Contacto[];
}