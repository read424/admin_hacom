import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from '../../environments/environment';
import { tap } from 'rxjs/operators';

const base_url = environment.url;


@Injectable({
  providedIn: 'root'
})
export class AdminService {

  constructor(private http: HttpClient) { }


    loginAdministrador(formData: any) {
      return this.http.post(`${base_url}user/controller.login.php?oper=login`, formData)
      .pipe(
        tap( (resp: any) => { 
          console.log(resp);
        })
      )
    }


 


}
