import { Injectable } from '@angular/core';
import { environment } from '../../environments/environment';
import { HttpClient } from '@angular/common/http';
import { tap } from 'rxjs/operators';


const base_url = environment.url;


@Injectable({
  providedIn: 'root'
})
export class ClientsService {

  constructor(private http: HttpClient) { }

  getClients() {
    return this.http.get(`${base_url}extras/controller.extras.php?oper=listarUsers`);
  }
 


  detailClient(id_cliente: any){
    // return this.http.post(`${base_url}user/controller.userlogin.php?oper=consultarUserAPP` , { id_cliente })
    return this.http.post(`${base_url}clientes/controller.clientes.php?oper=consultar` , { id_cliente })
    .pipe(
      tap((resp: any) => { 
        console.log(resp['data']);
      })
    )
  }

  detailUserApp(id_cliente: any){
     return this.http.post(`${base_url}extras/controller.extras.php?oper=consultarUserAPP` , { id_cliente })
     .pipe(
       tap((resp: any) => { 
         console.log(resp.rows[0]);
       })
     )
  }


  updateUserApp(id_cliente: any){
    return this.http.post(`${base_url}extras/controller.extras.php?oper=UpdateStatusLogin` ,  id_cliente )
 }
  

  getClientsERP() {
    return this.http.get(`${base_url}clientes/controller.clientes.php?oper=listar`);
  }


  getDepartamentos() {
    return this.http.get(`${base_url}catalogo/controller.departamentos.php?oper=listar`);
  }

  getProvincias() {
    return this.http.get(`${base_url}catalogo/controller.provincias.php?oper=listar`);
  }

  getDistritos() {
    return this.http.get(`${base_url}catalogo/controller.distritos.php?oper=listar`);
  }


  getRole(){
    return this.http.get(`${base_url}catalogo/controller.tipodocidentidad.php?oper=listar`); 
  }


 
  // GUARDAR O EDITAR CLIENTE DE COMERCIAL
  saveClient(formData: any){
    return this.http.post(`${base_url}clientes/controller.clientes.php?oper=guardar` ,  formData )
    .pipe(
      tap((resp: any) => { 
        console.log(resp);
      })
    )
 }


  
}
