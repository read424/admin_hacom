import { Injectable } from '@angular/core';
import { tap } from 'rxjs/operators';
import { environment } from '../../environments/environment';
import { HttpClient, HttpHeaders } from '@angular/common/http';

const base_url = environment.url;

@Injectable({
  providedIn: 'root'
})
export class VisitaService {



  constructor(private http: HttpClient) { }


  getVisitas(){
    return this.http.get(`${base_url}extras/controller.extras.php?oper=listarVisitas`)
  }

  detailVisita(id_visita: any){
    return this.http.post(`${base_url}extras/controller.extras.php?oper=consultarVisita` , { id_visita })
    .pipe(
      tap((resp: any) => { 
      })
    )
  }



  consultarVisitantes(id_visita: any){
    return this.http.post(`${base_url}extras/controller.extras.php?oper=consultarVisitantes` , { id_visita })
    .pipe(
      tap((resp: any) => { 
      })
    )
  }

  updateVisita(formData: any){
    return this.http.post(`${base_url}extras/controller.extras.php?oper=GuardarVisita` , formData)
    .pipe(
      tap((resp: any) => { 
      })
    )
  }




}
