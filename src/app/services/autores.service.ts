import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from '../../environments/environment';

const base_url = environment.url;

@Injectable({
  providedIn: 'root'
})

export class AutoresService {

  constructor(private http: HttpClient) { }

  getAuthors(){
    return [
      {
        id:1,
        tittle: 'Harry Pother',
        sinopsis: '', anio: 1990, name_author: '', publicado: ''
      },
      {
        id:2,
        tittle: 'Juego de Tronos',
        sinopsis: '', anio: 1996, name_author: 'George R. R. Martin', publicado: 2002
      },
      {
        id:3,
        tittle: 'Choque de Reyes',
        sinopsis: '', anio: 1998, name_author: 'George R. R. Martin', publicado: 1998
      },
      {
        id:4,
        tittle: 'Tormenta de Espadas',
        sinopsis: '', anio: 2000, name_author: 'George R. R. Martin', publicado: 2000
      },
      {
        id:5,
        tittle: 'Festín de cuervos',
        sinopsis: '', anio: 2005, name_author: 'George R. R. Martin', publicado: '21-10-2007'
      },
      {
        id:6,
        tittle: 'Danza de Dragones',
        sinopsis: '', anio: 2011, name_author: 'George R. R. Martin', publicado: '12-07-2011'
      },
    ];
  }

}
