import { HttpHeaders, HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { tap } from 'rxjs/operators';
import { environment } from '../../environments/environment';


const base_url = environment.url;


@Injectable({
  providedIn: 'root'
})
export class NotificationsService {

  constructor(private http: HttpClient) { }


  pushNotificationsAPP( title, content ,identificadores ){
    const post_data = { "notification": {
      "title": title,
      "body": content,
      "icon": "https://laimedev.com/proyectos2021/tlmapp/iconpush.jpg",
      "image": "https://laimedev.com/proyectos2021/tlmapp/123.jpg"
    },
      "registration_ids" : identificadores 
    }
    const httpOptions2 = {
      headers: new HttpHeaders({
          'Content-Type': 'application/json',
          'Authorization': 'key=AAAAWAKovGA:APA91bEQC1J4U2OY0G7l6M1X2SLRaoGdarTQq4SYsraD-SckELVA4YmArPIhqqs4wa0zOlKSbrqGo-PFwooze3E5cVvOlYp69o6RB9KL75oOwAOQ8P9vrlPeRIB4KizwVfFxspCp0Qhg',
      })
    };
    this.http.post('https://fcm.googleapis.com/fcm/send', post_data, httpOptions2)
    .subscribe(new_data => {
        console.log(new_data)
    }, error => {
        console.log(error);
    });
  }


  getPush() {
    return this.http.get(`${base_url}extras/controller.extras.php?oper=listarPush`);
  }


  savePush(formData: any) {
    return this.http.post(`${base_url}extras/controller.extras.php?oper=GuardarPush`, formData)
    .pipe(
      tap( (resp: any) => { 
        console.log(resp);
      })
    )
  }


}
